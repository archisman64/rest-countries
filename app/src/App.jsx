import React, { useState, useEffect } from 'react';
import { Routes, Route, Link } from 'react-router-dom';
import './App.css';
import Header from './components/Header';
import Filter from './components/Filter';
import Search from './components/Search';
import Card from './components/card';
import FilterSubregion from './components/FilterSubregion';
import Sort from './components/Sort';
import CountryDetails from './components/CountryDetails';

export const ThemeContext = React.createContext();

function App() {
  const [loading, setLoading] = useState(true);
  const [darkTheme, setDarkTheme] = useState(true);
  const [countries, setCountries] = useState([]);
  const [toShowCountries, setToShowCountries] = useState([]);

  const [filter, setFilter] = useState({ isActive: false, value: '' });
  const [search, setSearch] = useState({ isActive: false, value: '' });
  const [subregion, setSubregion] = useState('');
  const [sort, setSort] = useState('');

  const apiUrl = 'https://restcountries.com/v3.1/all';

  useEffect(() => {
    const getCountries = async () => {
      try {
        const result = await fetch(apiUrl);
        const jsonResult = await result.json();
        setCountries(jsonResult);
        setToShowCountries(jsonResult);
        setLoading(false);
      } catch (error) {
        console.log(error);
        setLoading(false);
      }
    };

    getCountries();
  }, []);

  function toggleTheme() {
    setDarkTheme(prevDarkTheme => !prevDarkTheme);
  }

  function searchACountry(inputString) {
    // console.log('search ran in app')
    let matchedCountries;
    if (subregion) {
      console.log('searching for country when subregion is active')
      matchedCountries = countries.filter(country => {
        // console.log('subregion:', country.subregion);
        if (country.subregion) {
          return country.subregion.toLowerCase() === subregion && country.name.common.toLowerCase().includes(inputString);
        }
      })
    } else if (filter.isActive) {
      matchedCountries = countries.filter(country => {
        return country.name.common.toLowerCase().includes(inputString.toLowerCase()) && country.region.toLowerCase() === filter.value;
      })
    } else {
      matchedCountries = countries.filter(country => {
        return country.name.common.toLowerCase().includes(inputString.toLowerCase());
      })
    }

    if (!inputString) {
      setSearch({ isActive: false, value: '' });
      // setToShowCountries(applySort(countries));
    } else {
      setSearch({ isActive: true, value: inputString.toLowerCase() });
    }
    setToShowCountries(applySort(matchedCountries));
  }


  function filterByRegion(region) {
    let filteredCountries;
    if (region === 'Filter by Region' || !region) {
      filteredCountries = [...countries];
      setFilter({ isActive: false, value: '' });
    } else {
      if (search.isActive) {
        filteredCountries = countries.filter(country => {
          return country.region.toLowerCase() === region.toLowerCase() && country.name.common.toLowerCase().includes(search.value);
        });
      } else {
        filteredCountries = countries.filter(country => {
          // if (country.region.toLowerCase === 'antarctic') {
          //   console.log('found in antarctic:', country);
          // }
          return country.region.toLowerCase() === region.toLowerCase();
        });
      }
      setFilter({ isActive: true, value: region });
    }
    setToShowCountries(applySort(filteredCountries));
  }

  // applies when sort by is changed
  function sortBySelection(sortBy) {
    if (sortBy === 'Sort By') {
      return;
    }
    const orderType = sortBy.split('-')[0];
    const orderCriteria = sortBy.split('-')[1].toLowerCase();
    let toSortCountries = [...toShowCountries];
    if (orderType === 'asc') {
      // console.log('sorting in ascending')
      setToShowCountries(toSortCountries.sort((countryA, countryB) => {
        return countryA[orderCriteria] - countryB[orderCriteria];
      }))
    } else {
      // console.log('sorting in descending')
      setToShowCountries(toSortCountries.sort((countryA, countryB) => {
        return countryB[orderCriteria] - countryA[orderCriteria];
      }))
    }
  }

  // applied every time when search or filter is changed
  function applySort(countries) {
    if (!sort || sort === 'Sort By') return countries;

    const orderType = sort.split('-')[0];
    const orderCriteria = sort.split('-')[1].toLowerCase();

    const modifiedCountries = [...countries];
    if (orderType === 'asc') {
      console.log('sorting in ascending')
      return modifiedCountries.sort((countryA, countryB) => {
        return countryA[orderCriteria] - countryB[orderCriteria];
      });
    } else {
      console.log('sorting in descending')
      return modifiedCountries.sort((countryA, countryB) => {
        return countryB[orderCriteria] - countryA[orderCriteria];
      });
    }
  }

  return (
    <>{loading ? <div>Loading...</div> : <ThemeContext.Provider value={darkTheme}>
      <div className='app'>
        <Header toggleTheme={toggleTheme}></Header>
        <Routes>
          <Route path='/' element={
            <>
              <div className={darkTheme ? 'bar dark' : 'bar'}>
                <Search onSearchAction={searchACountry}></Search>
                <Sort
                  onSortSelection={sortBySelection}
                  sort={sort}
                  setSort={setSort}
                ></Sort>
                <Filter
                  onRegionChange={filterByRegion}
                  countries={countries}
                ></Filter>
                <FilterSubregion
                  setToShowCountries={setToShowCountries}
                  filter={filter}
                  countries={countries}
                  setSubregion={setSubregion}
                  filterByRegion={filterByRegion}
                  search={search}
                  setSearch={setSearch}
                  applySort={applySort}
                ></FilterSubregion>
              </div>
              {toShowCountries.length > 0 ? (
                <div className={darkTheme ? 'cards-container dark' : 'cards-container'}>
                  {toShowCountries.map((country) => {
                    return <Card key={Math.random()} country={country}></Card>
                  })}
                </div>
              ) : (
                <p>No countries to show!</p>
              )}
            </>
          } />
          <Route path='/countries/:id' element={
            <CountryDetails countries={countries}></CountryDetails>
          } />
        </Routes>
      </div>
    </ThemeContext.Provider>}
    </>
  );
}

export default App;
