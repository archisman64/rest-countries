import { useContext } from 'react'
import { ThemeContext } from '../App';

const Search = ({ onSearchAction }) => {
    // const [currentInput, setCurrentInput] = useState('');
    const darkTheme = useContext(ThemeContext);

    function searchHandler(event) {
        // console.log('search handler ran')
        // setCurrentInput(event.target.value);
        onSearchAction(event.target.value);
    }

    return (
        <div className="search-box">
            <input className={darkTheme ? "search-input dark" : "search-input"} onChange={searchHandler} type="text" placeholder='Search for a country...' />
        </div>
    )
}

export default Search