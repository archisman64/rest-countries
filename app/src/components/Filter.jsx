import { useContext } from 'react'
import { ThemeContext } from '../App';

const Filter = ({ onRegionChange }) => {
    // const [selectedOption, setSelectedOption] = useState('');
    const darkTheme = useContext(ThemeContext);

    const filterHandler = (event) => {
        // console.log(event.target.value)
        const selectedValue = event.target.value;
        // console.log('region is changed');
        // console.log(event.target.value)

        // setSelectedOption(selectedValue);
        onRegionChange(selectedValue);
    }
    return (
        <div className="filter filter-region">
            <select className={darkTheme ? 'region-select dark' : 'region-select'} onChange={filterHandler} name="region" id="region">
                <option defaultValue="Filter by Region">Filter by Region</option>
                <option value="africa">Africa</option>
                <option value="americas">America</option>
                <option value="asia">Asia</option>
                <option value="europe">Europe</option>
                <option value="oceania">Oceania</option>
                {/* <option value="antarctic">Antarctic</option> */}
            </select>
        </div>
    )
}

export default Filter