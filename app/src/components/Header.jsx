import { useContext } from 'react'
import { ThemeContext } from '../App';
import './Header.css';
import './Search-filter.css';

const Header = ({ toggleTheme }) => {
    const darkTheme = useContext(ThemeContext);

    function themeHandler() {
        toggleTheme();
    }

    return (
        <header className={darkTheme ? 'header dark' : 'header'}>
            <div className='heading'>Where in the world?</div>
            <div className='mode'>
                <button className={darkTheme ? 'btn-dark dark' : 'btn-dark'} onClick={themeHandler}>{darkTheme ? <i className="fa-solid fa-moon fa-xs" style={{ color: "#ffffff" }}></i> : <i className="fa-regular fa-moon fa-xs"></i>} Dark Mode</button>
            </div>
        </header>
    )
}

export default Header