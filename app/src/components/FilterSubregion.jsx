import { useContext } from 'react'
import { ThemeContext } from '../App';

const FilterSubregion = ({
    setToShowCountries,
    filter,
    countries,
    setSubregion,
    filterByRegion,
    search,
    setSearch,
    applySort
}) => {
    const darkTheme = useContext(ThemeContext);

    const requiredRegion = filter.value;

    const requiredCountries = countries.filter(country => country.region.toLowerCase() === requiredRegion)

    const subregionSet = new Set(requiredCountries.map(country => country.subregion));

    const handleCountryDisplay = (event) => {
        console.log('handle country display ran')
        const selectedSubregion = event.target.value;

        if (!selectedSubregion) {
            console.log('no value selected')
            setSubregion('');
            return;
        }
        if (selectedSubregion === 'Filter by Subregion') {
            console.log('default value selected')
            filterByRegion(filter.value);
            setSubregion('');
            return;
        }
        console.log('general condition ran-> valid subregion is selected');
        if (search.isActive) {
            setToShowCountries(applySort(countries.filter(country => {
                if (country.subregion) {
                    return country.subregion.toLowerCase() === selectedSubregion && country.name.common.toLowerCase().includes(search.value);
                }
            })))
            return;
        }
        // if search field is empty
        setToShowCountries(applySort(countries.filter(country => {
            if (country.subregion) {
                return country.subregion.toLowerCase() === selectedSubregion;
            }
        })));
        setSubregion(selectedSubregion);
    }

    return (
        <div className="filter filter-subregion">
            <select className={darkTheme ? 'subregion-select dark' : 'subregion-select'} onChange={handleCountryDisplay} name="subregion" id="subregion">
                <option defaultValue="Filter by Subregion" >Filter by Subregion</option>
                {filter.value ? [...subregionSet].map((subregion) =>
                    <option key={subregion.toLowerCase()} value={subregion.toLowerCase()}>{subregion}</option>
                ) : <option value=''>Select a region first</option>}
            </select>
        </div>
    )
}

export default FilterSubregion;