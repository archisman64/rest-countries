import React from 'react'
import { useContext } from 'react'
import { ThemeContext } from '../App';
import { Link, useParams } from 'react-router-dom';
import './CountryDetails.css'

const CountryDetails = ({ countries }) => {
    const darkTheme = useContext(ThemeContext);
    const ccn = useParams().id;
    console.log('id:', ccn);
    console.log('countries:', countries)
    const requiredCountry = countries.find(country => country.ccn3 === ccn);
    console.log(requiredCountry);
    const borderCCA3s = requiredCountry.borders ? requiredCountry.borders : '';

    let borderCountries;
    if (borderCCA3s) {
        borderCountries = countries.reduce((acc, currCountry) => {
            if (borderCCA3s.includes(currCountry.cca3) && !acc[currCountry.name.common]) {
                acc[currCountry.name.common] = currCountry.ccn3;
            }
            return acc;
        }, {});
    }
    console.log('border countries:', borderCountries);

    return (
        <>
            {requiredCountry ? <div className={darkTheme ? 'main-container dark' : 'main-container'}>
                <Link className='btn-container' to={'/'}>
                    <button className={darkTheme ? 'btn-back dark' : 'btn-back'}>← Back</button>
                </Link>
                <div className='country-container'>
                    <div className='flag-container'>
                        <img src={requiredCountry.flags.png} alt="" />
                    </div>
                    <div className='data-container'>
                        <h2>{requiredCountry.name.common}</h2>
                        <div className='data-item native-name'><strong>Native Name: </strong>{Object.values(requiredCountry.name.nativeName)[0].common}</div>
                        <div className='data-item population'><strong>Population: </strong>{requiredCountry.population}</div>
                        <div className='data-item'><strong>Region: </strong>{requiredCountry.region}</div>
                        <div className='data-item'><strong>Sub Region: </strong>{requiredCountry.subregion}</div>
                        <div className='data-item'><strong>Capital: </strong>{requiredCountry.capital}</div>
                        <div className='data-item'><strong>Top Level Domain: </strong>{requiredCountry.tld.join(",")}</div>
                        <div className='data-item'><strong>Currencies: </strong>{requiredCountry.currencies ? Object.values(requiredCountry.currencies)[0].name : null}</div>
                        <div className='data-item'><strong>Languages: </strong>{requiredCountry.languages ? Object.values(requiredCountry.languages).join(",") : null}</div>
                        <div className='data-item border-countries'>
                            <span><strong>Border Countries: </strong></span>
                            {borderCountries ? <>
                                {Object.entries(borderCountries).map(([countryName, ccn3]) => {
                                    return <Link key={ccn3} to={`/countries/${ccn3}`}>
                                        <button className={darkTheme ? 'btn-country dark' : 'btn-country'}>{countryName}</button>
                                    </Link>;
                                })}
                            </> : 'No border countries'}
                        </div>
                    </div>
                </div>
            </div> : <div>Please wait...</div>}
        </>
    )
}

export default CountryDetails