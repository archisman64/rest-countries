import { useContext } from 'react'
import { ThemeContext } from '../App';
import { Link } from 'react-router-dom';
import './Card.css';

const card = ({ country }) => {
    const darkTheme = useContext(ThemeContext);

    return (
        <Link className='country-link' to={`/countries/${country.ccn3}`}>
            <div className={darkTheme ? 'country-card dark' : 'country-card'}>
                <div className='flag'>
                    <img src={country.flags.png} alt="flag-image" />
                </div>
                <div className={darkTheme ? 'data dark' : 'data'}>
                    <h4 className='name'>{country.name.common}</h4>
                    <div className='population'><strong>Population: </strong>{country.population}</div>
                    <div className='region'><strong>Region: </strong>{country.region}</div>
                    <div className='capital'><strong>Capital: </strong>{country.capital}</div>
                </div>
            </div>
        </Link>
    )
}

export default card