import { useContext } from 'react'
import { ThemeContext } from '../App';

const Sort = ({ onSortSelection, sort, setSort }) => {
    const darkTheme = useContext(ThemeContext);

    const sortOptions = [
        'Population ↑',
        'Population ↓',
        'Area ↑',
        'Area ↓'
    ];

    const sortCountries = (event) => {
        const selected = event.target.value;
        onSortSelection(selected);
        setSort(selected);
    }

    return (
        <div className="filter filter-sortby">
            <select className={darkTheme ? 'sort-select dark' : 'sort-select'} onChange={sortCountries}>
                <option defaultValue="sortby">Sort By</option>
                {sortOptions.map((element) => {
                    const orderType = element.split(' ')[1];
                    const orderCriteria = element.split(' ')[0];
                    return <option key={element} value={`${orderType === '↑' ? 'asc' : 'des'}-${orderCriteria}`}>{element}</option>
                })}
            </select>
        </div>
    )
}

export default Sort;